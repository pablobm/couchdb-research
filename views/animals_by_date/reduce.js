function(keys, values, rereduce) {
  log("REDUCE " + JSON.stringify([keys, values, rereduce]));
  if (rereduce) {
    return sum(values);
  } else {
    return values.length;
  }
}
