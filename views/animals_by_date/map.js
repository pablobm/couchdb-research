function (document) {
  const [date, _time] = document.date.split("T");
  const [year, month, day] = date.split("-");
  emit([year, month, day], 1);
}
