# CouchDB research sandbox

A Docker container hosting CouchDB, and some Ruby scripts to populate it with
data and design documents. I created this in order to experiment with CouchDB
and understand how map/reduce works.

Don't put this on production:

* CouchDB is running in "admin party" mode.
* There's no data persistence in place. When the Docker container stops,
  all data is lost.

## Running

* `rake run`: get show on the road. Will block the terminal and show CouchDB's
  output, which is useful for debugging.
* `rake populate`: fill DB with random documents and set up a design document.
* `rake query`: query the sample view and print out the results.

## Creating views

The task `rake populate` is a bit overengineered. Apart from generating random
documents, it will also create a design document called "default" which will
include views generates from files in the `views/` directory. This is the
structure it expects:

```
views
└── [name_of_view]
    ├── map.js
    └── reduce.js
```

It is also possible to specify that a view uses one of CouchDB's built-in
reduce functions. To do this, remove the `.js` extension from `reduce.js`
and have the file contain only the name of the built-in. For example:

```
# views/animals_by_date/reduce
_count
```

## Debugging

If you want to know what's going on inside a map or reduce function, use
CouchDB's built-in function `log` (not `console.log`, which doesn't work).
For example:

```
# views/animals_by_date/reduce.js
function(keys, values, rereduce) {
  log("REDUCE " + JSON.stringify([keys, values, rereduce]));
  # ...
}
```

Then check out the output of `rake run` for messages.
