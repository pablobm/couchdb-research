require "common"
require "couchdb"

couch = Couchdb.new(COUCHDB_BASE_URL, COUCHDB_DB_NAME)
result = couch.query_view(DEFAULT_DESIGN_DOC_NAME, :animals_by_date, group_level: 2)
pp result
